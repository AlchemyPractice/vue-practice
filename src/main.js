import Vue from 'vue'
import Axios from "axios"
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// json-server
Axios.defaults.baseURL = "http://localhost:3000";

// scss
require('@/assets/main.scss');

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
