module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData: '@import "./src/assets/prepends.scss";'
      }
    }
  }
};
